import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule } from '@angular/forms'; 
import { RouterModule, Routes } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { ReactiveFormsModule } from '@angular/forms'; 
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatButtonModule, MatCheckboxModule, MatFormFieldModule, MatInputModule, MatOptionModule, MatSelectModule} from '@angular/material';

import { AppComponent } from './app.component';
import { InterceptorAuthService } from './services/interceptor-auth.service';
import { authentificationGuard } from './guard/authentification.guard';
import { LoginComponent } from './login/login.component';
import { AdminComponent } from './admin/admin.component';

const appRoutes: Routes = [
  { path: 'home', component: AppComponent },
  { path: 'login', component: LoginComponent },
  { path: '', redirectTo: '/home', pathMatch: 'full'},
  { path: 'admin', component: AdminComponent},
]

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent
    AdminComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true }
    ),
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatButtonModule, 
    MatCheckboxModule, 
    MatFormFieldModule, 
    MatInputModule,
    MatOptionModule,
    MatSelectModule
  ],
  providers: [CookieService,
    { provide: HTTP_INTERCEPTORS, useClass: InterceptorAuthService, multi: true },
    authentificationGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
