import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

export interface illustationCom {
  category?: any,
  image: string,
  description: string,
  temoignage: string,
  id?: any
}

export interface Category {
  image: string,
  name: string,
  illustrationCommentee?: any,
  id?: any
}

@Injectable({
providedIn: 'root'
})
export class apiServiceService {

  constructor(private http: HttpClient) { }

  public getAll() {
    return this.http.get<Category[]>(environment.backUrl + 'api/');
  }

  public add(entity) {
    return this.http.post<illustationCom[]>(environment.backUrl + 'api/admin/add', entity);
  }

  public edit(id, entity) {
    return this.http.put<illustationCom[]>(environment.backUrl + 'api/admin/edit/' + id, entity);
  }

  public addCategory(entity) {
    return this.http.post<Category[]>(environment.backUrl + 'api/admin/category/add', entity);
  }

  public editCategory(id, entity) {
    return this.http.put<Category[]>(environment.backUrl + 'api/admin/category/edit/' + id, entity);
  }

  public remove(id) {
    return this.http.delete(environment.backUrl + 'api/admin/remove/' + id)
  }

  public removeCategory(id) {
    return this.http.delete(environment.backUrl + 'api/admin/category/remove/' + id)
  }
}