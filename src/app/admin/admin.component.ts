import { Component, OnInit } from '@angular/core';
import { apiServiceService } from '../services/api-service.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Base64 } from 'js-base64';
import { environment } from "../../environments/environment";

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  urlImg = environment.backUrl;
  categories;
  illustrationCommentee;
  modalCategory = false;
  modalIllustrationCom = false;
  addModeCat = true;
  addModeIllustrationCom = true;
  selectCategoryEdit;
  SelectillustrationCommenteeEdit;
  editCat: FormGroup;
  editIllustrationCommentee: FormGroup;
  addCat: FormGroup;
  addIllustrationCommentee: FormGroup;

  constructor(private apiService:apiServiceService, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.apiService.getAll().subscribe(
      data => { this.categories = data,
                console.log(this.categories)},
      error => console.log(error)
    )
    this.addCategory();
    this.editCategory();
    this.editIllustrationCom();
    this.addIllustrationCom();
  }

  editModeCat(id) {
    this.modalCategory = true; 
    this.selectCategoryEdit = id;
    this.addModeCat = false;
  }

  removeCategory(id) {
    this.apiService.removeCategory(id).subscribe();
  }

  addCategory() {
    this.addCat = this.formBuilder.group({
      image: ['', Validators.required],
      name: ['', Validators.required],
    })
  }

  onSubmitFormAddCategory() {
    let value = this.addCat.value;
    value.image = Base64.encode(value.image);
    console.log(value);
    this.apiService.addCategory(value).subscribe(
      () => { this.modalCategory = false },
      error => console.log(error));
  }


  addIllustrationCom() {
    this.addIllustrationCommentee = this.formBuilder.group({
      image: ['', Validators.required],
      description: ['', Validators.required],
      temoignage: ['', Validators.required],
      category: ['', Validators.required],
    })
  }

  onSubmitFormAddIllustrationCom() {
    let value = this.addIllustrationCommentee.value;
    this.apiService.addCategory(value).subscribe(
      () => { this.modalCategory = false },
      error => console.log(error));
  }

  editCategory() {
    this.editCat = this.formBuilder.group({
      image: ['', Validators.required],
      name: ['', Validators.required],
    })
  }

  onSubmitFormEditCategory() {
    let value = this.editCat.value;
    this.apiService.editCategory(this.selectCategoryEdit, value).subscribe(
      () => { this.modalCategory = false },
      error => console.log(error));
  }

  removeillustrationCommentee(id) {
    this.apiService.remove(id).subscribe();
  }

  editIllustrationCom() {
    this.editIllustrationCommentee = this.formBuilder.group({
      image: ['', Validators.required],
      description: ['', Validators.required],
      temoignage: ['', Validators.required],
      category: ['', Validators.required],
    })
  }

  onSubmitFormEditIllustrationCom() {
    let value = this.editCat.value;
    this.apiService.edit(this.SelectillustrationCommenteeEdit, value).subscribe(
      () => { this.modalIllustrationCom = false },
      error => console.log(error));
  }
}
